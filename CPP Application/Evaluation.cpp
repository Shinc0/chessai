//
// Created by Romain Pipon on 29/01/2022.
//

#include "Evaluation.h"

using namespace std;

bool isWhite(unsigned char c){
    return (c >= 65 && c <= 90);
}

int getPieceValue(unsigned char c){
    int value = 0;
    if(c == 'r' || c == 'R')
        value = ROOK_VALUE;
    else if(c == 'n' || c == 'N')
        value = KNIGHT_VALUE;
    else if(c == 'b' || c == 'B')
        value = BISHOP_VALUE;
    else if(c == 'q' || c == 'Q')
        value = QUEEN_VALUE;
    else if(c == 'k' || c == 'K')
        value = KING_VALUE;
    else if(c == 'p' || c == 'P')
        value = PAWN_VALUE;
    return value;
}

int evaluate(string fen, bool color){
    int whiteScore = 0;
    int blackScore = 0;

    for(char c : fen){
        if(isWhite(c))
            whiteScore+=getPieceValue(c);
        else
            blackScore+=getPieceValue(c);
    }

    if(color == WHITE)
        return (blackScore-whiteScore);
        //return (whiteScore-blackScore);
    else
       // return (blackScore-whiteScore);
        return (whiteScore-blackScore);
}