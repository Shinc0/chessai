//
// Created by Romain Pipon on 27/02/2022.
//

#ifndef CHESS_BOOK_H
#define CHESS_BOOK_H

#include <string>
#include <fstream>
#include <vector>

using namespace std;

class Book {
    string _filename;
    vector<string> _playedMoves;
public:
    Book(string filename,vector<string> playedMoves);
    string getNextMove();
};


#endif //CHESS_BOOK_H
