/**
 * @file Chess.h
 * @brief Engine for chess game
 * @author Romain PIPON and Julien GODICHEAU
 * @version 1.0
 * @date 28/01/2022
 *
 * This class is an engine for the Chess game
 */

#ifndef CHESS_CHESS_H
#define CHESS_CHESS_H

/*! \def    BLACK
    \brief  Value set for black piece*/
#define BLACK 0
/*! \def    WHITE
    \brief  Value set for white piece*/
#define WHITE 1
/*! \def    NONE
    \brief  Value set for empty case*/
#define NONE -1

/*! \def    KING
    \brief  Symbol set for king piece*/
#define KING "K"
/*! \def    QUEEN
    \brief  Symbol set for queen piece*/
#define QUEEN "Q"
/*! \def    ROOK
    \brief  Symbol set for rook piece*/
#define ROOK "R"
/*! \def    KNIGHT
    \brief  Symbol set for knight piece*/
#define KNIGHT "N"
/*! \def    BISHOP
    \brief  Symbol set for bishop piece*/
#define BISHOP "B"
/*! \def    PAWN
    \brief  Symbol set for pawn piece*/
#define PAWN "P"
/*! \def    EMPTY
    \brief  Symbol set for empty case*/
#define EMPTY "."

#include <string>
#include <vector>

using namespace std;

/*!  \class     Piece
     \brief     This class can managed a Piece for the chess game. The class allows basic operations like create a Piece, get is type, get is color,
                get is symbol...
*/
class Piece {
    private:
        string _type;
        bool _color;

    public:
        /*Constructor*/
        Piece(string type, bool color);
        Piece();

        void          setType(string type) { _type = type;   } //Set the type of the piece
        string        getType()            { return _type;   } //Get the type of the piece
        void          setColor(bool color) { _color = color; } //Set the color of the piece
        bool          getColor()           { return _color;  } //Get the color of the piece
        unsigned char getSymbol();                             //Get the symbol of the pice
};



/*!  \class     Chess
     \brief     This class can managed the board, and movement for the chess game. The class allows basic operations like create a new Game,
                generate all move possible for a player, make and undo a move, know if a king is in check etc...
*/
class Chess {
    private:
        bool _sideToMove = WHITE;      //The side that need to play
        char _en_passant = -1;        //Save the index of the case where an en passant move is possible
        int _ply         =  0;       //Save the amount of hits without take (fifty-move rule)
        int _move_number =  0;      //Save the amount of move since the start of the game
        bool _gameover   =  false; //Flag to know if the game is over (Any move is possible, fifty-move rule or leave)
        bool _moved = false;
        vector<string> _history; //Save the fen of all the move in historic useful for undo move

        bool _white_can_castle_56 =false; //Small Roque
        bool _white_can_castle_63 =false; //Big Roque
        bool _black_can_castle_0  =false; //Small Roque
        bool _black_can_castle_7  =false; //Big Roque

        //MailBox, use to avoid overflow during the movement of the piece
        char _tab120[120] = {
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1,  0,  1,  2,  3,  4,  5,  6,  7, -1,
                -1,  8,  9, 10, 11, 12, 13, 14, 15, -1,
                -1, 16, 17, 18, 19, 20, 21, 22, 23, -1,
                -1, 24, 25, 26, 27, 28, 29, 30, 31, -1,
                -1, 32, 33, 34, 35, 36, 37, 38, 39, -1,
                -1, 40, 41, 42, 43, 44, 45, 46, 47, -1,
                -1, 48, 49, 50, 51, 52, 53, 54, 55, -1,
                -1, 56, 57, 58, 59, 60, 61, 62, 63, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
        };

        //Lookup table for know the index of a cell into '_tab120'
        char _tab64[64] = {
                21, 22, 23, 24, 25, 26, 27, 28,
                31, 32, 33, 34, 35, 36, 37, 38,
                41, 42, 43, 44, 45, 46, 47, 48,
                51, 52, 53, 54, 55, 56, 57, 58,
                61, 62, 63, 64, 65, 66, 67, 68,
                71, 72, 73, 74, 75, 76, 77, 78,
                81, 82, 83, 84, 85, 86, 87, 88,
                91, 92, 93, 94, 95, 96, 97, 98
        };

        //Lookup table useful to know the index of a cell
        string _coords[64]={
                "a8","b8","c8","d8","e8","f8","g8","h8",
                "a7","b7","c7","d7","e7","f7","g7","h7",
                "a6","b6","c6","d6","e6","f6","g6","h6",
                "a5","b5","c5","d5","e5","f5","g5","h5",
                "a4","b4","c4","d4","e4","f4","g4","h4",
                "a3","b3","c3","d3","e3","f3","g3","h3",
                "a2","b2","c2","d2","e2","f2","g2","h2",
                "a1","b1","c1","d1","e1","f1","g1","h1"
        };

        //Chessboard
        Piece _cells[64]={
            Piece(ROOK,BLACK),Piece(KNIGHT,BLACK),Piece(BISHOP,BLACK),Piece(QUEEN,BLACK),Piece(KING,BLACK),Piece(BISHOP,BLACK),Piece(KNIGHT,BLACK),Piece(ROOK,BLACK),
            Piece(PAWN,BLACK),Piece(PAWN,BLACK),Piece(PAWN,BLACK),Piece(PAWN,BLACK),Piece(PAWN,BLACK),Piece(PAWN,BLACK),Piece(PAWN,BLACK),Piece(PAWN,BLACK),

            Piece(),Piece(),Piece(),Piece(),Piece(),Piece(),Piece(),Piece(),
            Piece(),Piece(),Piece(),Piece(),Piece(),Piece(),Piece(),Piece(),
            Piece(),Piece(),Piece(),Piece(),Piece(),Piece(),Piece(),Piece(),
            Piece(),Piece(),Piece(),Piece(),Piece(),Piece(),Piece(),Piece(),

            Piece(PAWN,WHITE),Piece(PAWN,WHITE),Piece(PAWN,WHITE),Piece(PAWN,WHITE),Piece(PAWN,WHITE),Piece(PAWN,WHITE),Piece(PAWN,WHITE),Piece(PAWN,WHITE),
            Piece(ROOK,WHITE),Piece(KNIGHT,WHITE),Piece(BISHOP,WHITE),Piece(QUEEN,WHITE),Piece(KING,WHITE),Piece(BISHOP,WHITE),Piece(KNIGHT,WHITE),Piece(ROOK,WHITE)
        };

        //Moving vectors according to the '_tab64' and '_tab120'
        char _move_vectors_bishop[4] = {-11,-9,9,11};
        char _move_vectors_rook[4]   = {-10,-1,1,10};
        char _move_vectors_knight[8] = {-21,-19,-12,-8,8,12,19,21};

    public:
        Chess();                                                //Constructor
        void draw();                                            //Drawn the chessboard in the terminal
        void setBoard(string fen);                              //Set the board with a FEN
        string getBoard();                                      //Get the board in fen-format
        int getIndex(string c);                                 //Get the index of cell e.g (e4 -> 36)
        bool opponentColor(bool color);                         //Get the opponent color
        void changeTrait();                                     //Change the side to move
        bool getSideToMove();                                   //Get the player that need to play
        void pos_pawn(int index,bool color, vector<int> *pm);   //Get the possible move for the pawn piece
        void pos_rook(int index,bool color, vector<int> *pm);   //Get the possible move for the rook piece
        void pos_bishop(int index,bool color, vector<int> *pm); //Get the possible move for the bishop piece
        void pos_knight(int index, bool color, vector<int> *pm);//Get the possible move for the kight piece
        void pos_roque(int index, bool color, vector<int> *pm); //Get the possible move for make a roque
        bool isAttacked(int pos, bool color);                   //Know if a piece is attacked by the possible move of the opponent player
        bool inCheck(bool color);                               //Know if a king is in check
        void genMovesList(bool color, vector<string> *pm);      //Generate all possible moves for a side
        void genLegalMoves(bool color, vector<string> *pm);     //Generate all legal moves
        void undo();                                            //Undo move
        char move(string move);                                 //Make a move e.g ("e2e4" -> the pawn from the cell e2 is moved to e4)
        int gameover();                                         //Know if a game is over
        void save();                                            //Save the board with the fen description
        char makeMove(string move);                             //Make a move e.g ("e2e4" -> the pawn from the cell e2 is moved to e4) with error control
        int getAmountOfLegalMove(bool color);                   //Get the amount of legal for a side
};

#endif //CHESS_CHESS_H
