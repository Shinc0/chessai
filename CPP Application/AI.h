/**
 * @file AI.h
 * @brief AI class
 * @author Romain PIPON and Julien GODICHEAU
 * @version 1.0
 * @date 04/02/2022
 */


#ifndef CHESS_AI_H
#define CHESS_AI_H

#include "Chess.h"
#include "Evaluation.h"
#include "Book.h"
#include  <vector>
#include <stdlib.h>
#include <time.h>
#include <thread>

#define OK 1

/*!  \class     AI
     \brief     This class manage the AI to play the chess game.
            
*/
class AI {
private:
    Chess * _game;
    int _depth;
    bool _readBookFlag;
    vector<string> *_moves;

public:
    AI(Chess *game, int depth, bool readBookFlag, vector<string> *moves);
    int AlphaBeta(Chess *game, vector<string> ml,int depth, bool sideToMove,string *pm,int Alpha, int Beta);
    int play();
    int search();
    int ab(Chess *game, int depth,int alpha, int beta,string *pm);

};


#endif //CHESS_AI_H
