//
// Created by Romain Pipon on 27/02/2022.
//

#include "Book.h"
#include "iostream"

using namespace std;

vector<string> splt(string s, string delimiter) {
    size_t pos_start = 0, pos_end, delim_len = delimiter.length();
    string token;
    vector<string> res;

    while ((pos_end = s.find (delimiter, pos_start)) != string::npos) {
        token = s.substr (pos_start, pos_end - pos_start);
        pos_start = pos_end + delim_len;
        res.push_back (token);
    }

    res.push_back (s.substr (pos_start));
    return res;
}

vector<string> splitAt(vector<string> s, int index){
    vector<string> string;
    for (int i = 0; i < index; ++i) {
        string.push_back(s[i]);
    }
    return string;
}

Book::Book(string filename, vector<string> playedMoves) {
    _filename = filename;
    _playedMoves = playedMoves;
}

string Book::getNextMove() {
    FILE* fp = fopen(_filename.c_str(), "r");
    if (fp == NULL)
        exit(EXIT_FAILURE);

    char* line = NULL;
    size_t len = 0;
    int score=0, bestScore=0;

    string bestMove;

    while ((getline(&line, &len, fp)) != -1) {
        if(strstr(line,_playedMoves[0].c_str())){

            vector<string> l = splt(line, " ");

            if(splitAt(l, _playedMoves.size()) == _playedMoves){
                score = l.size();
                if(score >= bestScore){
                    bestScore = score;
                    bestMove = l[_playedMoves.size()];
                }
                if(score >=23){
                    break;
                }
            }
        }

    }
    //printf("%s", bestMove.c_str());
    fclose(fp);
    if (line)
        free(line);

    return bestMove;
}