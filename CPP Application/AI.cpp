/*
* @file Chess.cpp
* @brief Class for managing the ai
* @author Romain PIPON and Julien GODICHEAU
* @version 1.0
* @date 28/01/2022
*/


#include "AI.h"
#include "iostream"

/**
    \brief      Constructor of the class AI. Initialize the AI with the game and the depth.
*/

AI::AI(Chess *game, int depth, bool readBookFlag, vector<string> *moves) {
    _game = game;
    _depth = depth;
    _readBookFlag = readBookFlag;
    _moves = moves;
    srand(time(NULL));
}


/** \brief     This function is the main function of the AI. It call the AlphaBeta function. It's a recursive function.
 *
 * \param[in] game The game
 * \param[in] depth The depth of the tree
 * \param[in] sideToMove The side to move
 * \param[in] pm The pointer to the variable that will contain the best move
 * \param[in] Alpha The alpha value
 * \param[in] Beta The beta value
 *
 * \return **[int]** The best move of the AI.
 */
int AI::AlphaBeta(Chess *game,vector<string> ml,int depth, bool sideToMove,string *pm,int Alpha, int Beta){

    if(game->gameover()){
        if(game->getAmountOfLegalMove(WHITE) < 1 && game->inCheck(WHITE)){ //If white is in checkmate
            return  -200000000-depth;
        }
        else if(game->getAmountOfLegalMove(BLACK) < 1 && game->inCheck(BLACK)){ //If black is in checkmate
            return  +200000000+depth;
        }
        else
            return 0; //Draw or pat
    }

    if(depth == 0)
        return evaluate(game->getBoard(), game->opponentColor(game->getSideToMove())); //Evaluation of the board


    int bestScore;
    string bestMove;

    bestScore = -200000000;

    //For each move in the move list
    for (string move : ml) {

        if(game->makeMove(move) == 1){ //If the move is possible and make the move

            //Gen the new moves possible
            vector<string> moves;
            game->genLegalMoves(game->getSideToMove(), &moves);

            //Call alpha-beta
            int score = -AlphaBeta(game,moves,depth-1,game->getSideToMove(),pm,-Alpha,-Beta);

            //Undo the last move
            game->undo();

            if(score > bestScore){

                bestScore = score;
                bestMove = move;

                if(score>=Alpha){

                    Alpha = score;

                    if(Alpha>=Beta){

                        *pm = bestMove;
                        return bestScore;

                    }
                }
            }
        }
    }

    *pm = bestMove;
    return bestScore;
}


/** \brief Call this function to let the AI play.
 *
 * \return **[string]** return the move that the AI play.
 */
int AI::play(){
    string move;

    if(_readBookFlag){
        Book *book = new Book("book.txt",*_moves);

        move = book->getNextMove();
        if(move.size() <= 1)
            _readBookFlag = false;
    }
    if(_readBookFlag == false){
        vector<string> moves;
        _game->genLegalMoves(BLACK, &moves);
        AlphaBeta(_game,moves,_depth,BLACK, &move,-200000000, 200000000);
    }
    if(move != "" && !_game->gameover()){
        while (_game->makeMove(move) != 1 );
        return 1;
    } else{
        if(_game->getAmountOfLegalMove(WHITE) < 1 && _game->inCheck(WHITE)){ //If white is in checkmate
            return  -1;
        }
        else if(_game->getAmountOfLegalMove(BLACK) < 1 && _game->inCheck(BLACK)){ //If black is in checkmate
            return  -2;
        }
            return 0; //Draw or pat
    }
        return 0;
}

