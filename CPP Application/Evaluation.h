//
// Created by Romain Pipon on 29/01/2022.
//

#ifndef CHESS_EVALUATION_H
#define CHESS_EVALUATION_H

#define BLACK 0
#define WHITE 1

#define KING_VALUE 10
#define PAWN_VALUE 1
#define BISHOP_VALUE 3
#define KNIGHT_VALUE 3
#define ROOK_VALUE 5
#define QUEEN_VALUE 9

#include <string>

bool isWhite(unsigned char c);
int getPieceValue(unsigned char c);
int evaluate(std::string fen, bool color);


#endif //CHESS_EVALUATION_H
