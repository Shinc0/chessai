#include <iostream>
#include "Chess.h"
#include "AI.h"
#include "Evaluation.h"

using namespace std;

int main( int argc, char *argv[], char *envp[] ){
    Chess *game = new Chess();                                              //Create new game

    vector<string> playedMove;                                              //Is used to saved the played moves and used to read the book move if the readBookFlag is set to true
    AI *ai = new AI(game,3, false, &playedMove); //Create new AI with a depth of 2, it's mean that the AI is able to see 1 move for black and 1 move for white

    if(argc > 1){                                     //If a FEN is set in argument
        game->setBoard(argv[1]);                     //Set the board with the fen
        if(game->getSideToMove() == BLACK){         //AI play the black
            int status = ai->play();               //Play a movement and return the status
            if(status == OK)                      //If the AI is able to play
                cout<< game->getBoard() << endl; //Send the new fen
            else
                cout << status << endl;         //Send the error or end game status
        }
    }
    else{
        cout << "The correct usage: ./Chess [FEN]" << endl;
    }

    delete ai;
    delete game;
    return 0;
}
