/**
 * @file Chess.cpp
 * @brief Class for managing the Chess game
 * @author Romain PIPON and Julien GODICHEAU
 * @version 1.0
 * @date 28/01/2022
 */

#include "Chess.h"
#include <iostream>
using namespace std;



/**
    \brief      Constructor of the class Piece. Create a piece with a type and a color
*/
Piece::Piece(string type, bool color) {
    _type = type;
    _color = color;
}

/**
    \brief      Constructor of the class Piece. Create a empty piece for an empty cell
*/
Piece::Piece() { //Empty Piece
    _type = EMPTY;
    _color = NONE;
}

/** \brief Get the symbol of the piece
 *
 * \return **[unsigned char]** return the char of the piece in **uppercase** if the piece is WHITE else in **lowercase** if the piece is BLACK
 */
unsigned char Piece::getSymbol() {
    if(_color == WHITE || _color == NONE)
        return _type[0]; //return the letter in uppercase for the white
    else
        return _type[0] + 32; //return the letter in lowercase for the black
}

//**********************************************************************************************************************
//**********************************************************************************************************************
//**********************************************************************************************************************

/** \brief Split a string
 *
 * \param[in] s The string to split
 * \param[in] delimiter Split the string at each delimiter
 *
 * \return **[vector<string>]** return a vector/array of the splited string
 */
vector<string> split(string s, string delimiter) {
    size_t pos_start = 0, pos_end, delim_len = delimiter.length();
    string token;
    vector<string> res;

    while ((pos_end = s.find (delimiter, pos_start)) != string::npos) {
        token = s.substr (pos_start, pos_end - pos_start);
        pos_start = pos_end + delim_len;
        res.push_back (token);
    }

    res.push_back (s.substr (pos_start));
    return res;
}

/** \brief Know if a char is uppercase.
 *
 * \param[in] s The string to split
 *
 * \return **[bool]** return a **true** if the char is in uppercase, **false** if the char is in lowercase
 */
bool isUppercase(unsigned char c){
    return (c >= 65 && c <= 90);
}

/** \brief Know the piece type with this symbol
 *
 * \param[in] c The symbol
 *
 * \return **[string]** return the type
 */
string symbolToType(unsigned char c){
    string type = EMPTY;
    if(c == 'r' || c == 'R')
        type = ROOK;
    else if(c == 'n' || c == 'N')
        type = KNIGHT;
    else if(c == 'b' || c == 'B')
        type = BISHOP;
    else if(c == 'q' || c == 'Q')
        type = QUEEN;
    else if(c == 'k' || c == 'K')
        type = KING;
    else if(c == 'p' || c == 'P')
        type = PAWN;

    return type;
}

/**
    \brief Constructor of the class Chess. Create a game, initialize the board and the side to move
*/
Chess::Chess() {
    _sideToMove = WHITE;
    setBoard("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
}

/**
    \brief      Draw the chessboard in text mode (console mode). This method uses cout to draw the game, it assumes that cout can be used.
    \todo       It would be better to return a string
*/
void Chess::draw() {
    int rows = 8, i=0;
    for (Piece p : _cells) {
        i++;
        cout << " " << p.getSymbol();
        /*Show the num of the row at the right*/
        if(!(i%8)){
            cout << " \e[0;31m" << rows << "\e[0m\n";
            rows--;
        }
    }
    /*Show the letter at the bottom*/
    for (int i = 0; i < 8; ++i) {
        unsigned char c = 97 +i;
        cout << " \e[0;31m" << c;
    }
    cout << "\e[0m\n\n";
}

/** \brief Set the chessboard with a fen
 *
 * \param[in] fen The board to set in fen format
 *
 * \return **[string]** return the type [\ref KING, \ref QUEEN, \ref ROOK, \ref BISHOP, \ref KNIGHT, \ref PAWN, \ref EMPTY]
 */
void Chess::setBoard(string fen) {
    //[pieces] [side to move] [castle rights] [ep] [plys] [move number]

    unsigned int index = 0;
    vector<string> fen_splited = split(fen, " ");

    /*Parse the board and set the FEN*/
    for(char c : fen_splited[0]){
        if(c != '/'){
            if((c >= 65 && c <= 90) || (c>=97 && c <=122)){ // If it's a letter
                _cells[index].setType(symbolToType(c));
                _cells[index].setColor(isUppercase(c));
                index+=1;
            }
            else{
                for(int i=0; i<(c-'0');i++){
                    _cells[index].setType(EMPTY);
                    _cells[index].setColor(NONE);
                    index++;
                }
            }
        }
    }

    /*Side to move*/
    if(fen_splited[1] == "w")
        _sideToMove = WHITE;
    else
        _sideToMove = BLACK;

    /*Right to castling*/
    string castling = fen_splited[2];
    if(castling != "-"){
        if(castling.find("K")<castling.length())
            _white_can_castle_63 = true;
        else
            _white_can_castle_63 = false;

        if(castling.find("Q")<castling.length())
            _white_can_castle_56 = true;
        else
            _white_can_castle_56 = false;

        if(castling.find("k")<castling.length())
            _black_can_castle_7 = true;
        else
            _black_can_castle_7 = false;

        if(castling.find("q")<castling.length())
            _black_can_castle_0 = true;
        else
            _black_can_castle_0 = false;
    }
    else{
        _white_can_castle_63 = false;
        _white_can_castle_56 = false;
        _black_can_castle_7 = false;
        _black_can_castle_0 = false;
    }

    /*En passant*/
    for (string coord:_coords) {
        if(fen_splited[3] == coord){
            _en_passant = getIndex(fen_splited[3]);
            break;
        }
        _en_passant = -1;
    }

    /*Amount of hits*/
    _ply = stoi(fen_splited[4]);

    /*Move Number*/
    _move_number = stoi(fen_splited[5]);;
}

/** \brief Get the chess board in fen format
 *
 *\return **[string]** return the chessboard to the fen format
 */
string Chess::getBoard() {
    string fen = "";

    /*Get the board*/
    int emptySq = 0;
    for (int i = 0; i < 64; ++i) {
        if(emptySq==8){
            fen+='8';
            emptySq=0;
        }
        if(i && (i%8)==0){
            if(emptySq>0){
                fen+=(emptySq+'0');
                emptySq=0;
            }
            fen+='/';
        }
        if(_cells[i].getType() == EMPTY)
            emptySq++;
        else{
            if(emptySq>0){
                fen+=(emptySq+'0');
                emptySq=0;
            }
            fen+= _cells[i].getSymbol();
        }

    }
    if(emptySq>0)
        fen+=(emptySq+'0');

    /*Set the side to move*/
    fen+= " ";
    if(_sideToMove == WHITE)
        fen+="w";
    else
        fen+="b";

    /*Set the right to castling*/
    fen+= " ";
    if(_white_can_castle_63)
        fen+="K";
    if(_white_can_castle_56)
        fen+="Q";
    if(_black_can_castle_7)
        fen+="k";
    if(_black_can_castle_0)
        fen+="q";
    if((!_white_can_castle_56 && !_white_can_castle_63 && !_black_can_castle_7 && !_black_can_castle_0))
        fen+="-";

    /*En passant*/
    fen+= " ";
    if(_en_passant != -1)
        fen+=_coords[_en_passant];
    else
        fen+="-";

    /*50 move rule*/
    fen+=" ";
    fen+=to_string(_ply);

    /*Move Number*/
    fen+= " ";
    fen+=to_string(_move_number);

    return fen;
}

/** \brief Get the index of cell **e.g:** (e4 -> 36).\n
 *         Use the '_coords' as a lookup table
 *
 * \param[in] c The cell **e.g:** "e4"
 *
 * \return **[int]** return the index of the cell
 */
int Chess::getIndex(string c) {
    /*Find the index of the cell*/
    for (int i = 0; i<64 ; ++i)
        if(_coords[i] == c)
            return i;
    return -1;
}

/** \brief Know the opponent color\n
 * **e.g:** BLACK return WHITE and WHITE return BLACK
 *
 * \param[in] color The color of the current player
 *
 * \return **[string]** return the opponent color
 */
bool Chess::opponentColor(bool color){
    if(color==WHITE)
        return BLACK;
    else
        return WHITE;
}

/**
 * \brief Change the side to move
 */
void Chess::changeTrait() {
    if(_sideToMove == WHITE)
        _sideToMove = BLACK;
    else
        _sideToMove = WHITE;
}

/** \brief Get the side to move/play
 *
 * \return **[bool]** return the side to move \ref WHITE or \ref BLACK
 */
bool Chess::getSideToMove() {
    return _sideToMove;
}

/** \brief Generate the possible cell where the pawn can go
 *
 * \param[in] index The index/location of the pawn
 *
 * \param[in] color The color of the current player
 *
 * \param[out] pm The pointer of the vector/array to save the data
 *
 */
void Chess::pos_pawn(int index,bool color, vector<int> *pm){
    if(color == WHITE){

        if(_cells[(index-8)].getType() == EMPTY){ //If no piece in front of

            pm->push_back((index-8)); //Save the cell

            if(index >=48 && index <=55 && _cells[(index-16)].getType() == EMPTY) { //First move, can reach 2
                pm->push_back((index - 16)); //Save the cell
            }
        }

        /*Pawn taking opponent*/
        if(_tab120[_tab64[index]-11] != -1){ //No overflow
            if(_cells[(index-9)].getColor() == BLACK ||((index-9) == _en_passant && _en_passant >= 16 && _en_passant <= 23)) //if opponent piece or en passant move
                pm->push_back((index-9)); //Save the cell
        }

        if(_tab120[_tab64[index]-9] != -1){ //No overflow
            if(_cells[(index-7)].getColor() == BLACK ||((index-7) == _en_passant && _en_passant >= 16 && _en_passant <= 23)) //if opponent piece  or en passant move
                pm->push_back((index-7)); //Save the cell
        }

    }
    else if (color == BLACK){
        if(_cells[(index+8)].getType() == EMPTY){ //If no piece in front of
            pm->push_back((index+8));
            if(index >=8 && index <=15 && _cells[(index+16)].getType() == EMPTY){ //First move, can reach 2
                pm->push_back((index+16));
            }

        }
        if(_tab120[_tab64[index]+11] != -1){ //No overflow
            if(_cells[(index+9)].getColor() == WHITE && _cells[(index+9)].getType() != EMPTY || ((index+9) == _en_passant && _en_passant >= 40 && _en_passant <= 47)) //if opponent piece
                pm->push_back((index+9));
        }
        if(_tab120[_tab64[index]+9] != -1){ //No overflow
            if(_cells[(index+7)].getColor() == WHITE && _cells[(index+7)].getType() != EMPTY || ((index+7) == _en_passant && _en_passant >= 40 && _en_passant <= 47)) //if opponent piece
                pm->push_back((index+7));
        }
    }
}

/** \brief Generate the possible cell where the rook can go
 *
 * \param[in] index The index/location of the rook
 *
 * \param[in] color The color of the current player
 *
 * \param[out] pm The pointer of the vector/array to save the data
 *
 */
void Chess::pos_rook(int index,bool color, vector<int> *pm){
    int j=1;
    for (char k : _move_vectors_rook){
        j=1;
        while (true){

            int n = _tab120[_tab64[index] + (k*j)];

            if(n!=-1){ //Overflow
                if(_cells[n].getType() == EMPTY || _cells[n].getColor() != color) //If is not empty and it's an opponent piece
                    pm->push_back(n); //save the cell
            }
            else
                break;

            if(_cells[n].getType() != EMPTY || _cells[index].getType() == KING) //if the cell is not empty or the piece to generate the movement is the king
                break;
            j++;
        }
    }
}

/** \brief Generate the possible cell where the bishop can go
 *
 * \param[in] index The index/location of the bishop
 *
 * \param[in] color The color of the current player
 *
 * \param[out] pm The pointer of the vector/array to save the data
 *
 */
void Chess::pos_bishop(int index, bool color, vector<int> *pm) {
    int j=1;
    for (char k : _move_vectors_bishop){
        j=1;
        while (true){
            int n = _tab120[_tab64[index] + (k*j)];
            if(n!=-1){
                if(_cells[n].getType() == EMPTY || _cells[n].getColor() != color) //If is not empty and it's an opponent piece
                    pm->push_back(n); //save the cell
            }
            else
                break;
            if(_cells[n].getType() != EMPTY || _cells[index].getType() == KING)//if the cell is not empty or the piece to generate the movement is the king
                break;
            j++;
        }
    }
}

/** \brief Generate the possible cell where the knight can go
 *
 * \param[in] index The index/location of the knight
 *
 * \param[in] color The color of the current player
 *
 * \param[out] pm The pointer of the vector/array to save the data
 *
 */
void Chess::pos_knight(int index, bool color, vector<int> *pm){
    for(char i : _move_vectors_knight){
        int _case = _tab120[_tab64[index]-i];
        if(_case != -1){
            if(_cells[_case].getColor() != color || _cells[_case].getType() == EMPTY)
                pm->push_back(_case); //Save the cell
        }
    }

}

/** \brief Generate the roque movement
 *
 * \param[in] index The index/location of the king
 *
 * \param[in] color The color of the current player
 *
 * \param[out] pm The pointer of the vector/array to save the data
 *
 */
void Chess::pos_roque(int index, bool color, vector<int> *pm) {
    if(_cells[index].getType() == KING){
        if(color == WHITE ){
            if(_white_can_castle_63 && _cells[60].getType() == KING &&_cells[61].getType() == EMPTY && _cells[62].getType() == EMPTY && _cells[63].getType() == ROOK) //Petit roque
                pm->push_back(62);
            if(_white_can_castle_56 && _cells[60].getType() == KING &&_cells[59].getType() == EMPTY && _cells[58].getType() == EMPTY && _cells[57].getType() == EMPTY && _cells[56].getType() == ROOK) //Grand roque
                pm->push_back(58);
        }
        else{
            if(_black_can_castle_7 && _cells[4].getType() == KING &&_cells[5].getType() == EMPTY && _cells[6].getType() == EMPTY && _cells[7].getType() == ROOK) //Petit roque
                pm->push_back(6);
            if(_black_can_castle_0 && _cells[4].getType() == KING &&_cells[3].getType() == EMPTY && _cells[2].getType() == EMPTY && _cells[1].getType() == EMPTY && _cells[0].getType() == ROOK) //Grand roque
                pm->push_back(2);
        }
    }
}

/** \brief Generate all possible move for a player/color
 *
 * \param[in] color The color of the player we want to generate all the possible move
 *
 * \param[out] pm The pointer of the vector/array to save the data
 *
 */
void Chess::genMovesList(bool color, vector<string> *pm){
    for (int i = 0; i < 64; ++i) { //for each piece in the chessboard
        Piece p = _cells[i];
        if(p.getColor() == color){
            if(p.getType() == PAWN){ //Gen move for pawn
                vector<int> pos;
                pos_pawn(i, color,&pos);
                for(int p : pos){
                    string move = _coords[i];
                    move+=_coords[p];
                    pm->push_back(move); //Save the movement
                }

            }
            if(p.getType() == KNIGHT){ //Gen move for knight
                vector<int> pos;
                pos_knight(i, color,&pos);
                for(int p : pos){
                    string move = _coords[i];
                    move+=_coords[p];
                    pm->push_back(move); //Save the movement
                }
            }
            if(p.getType() == BISHOP){ //Gen move for bishop
                vector<int> pos;
                pos_bishop(i, color,&pos);
                for(int p : pos){
                    string move = _coords[i];
                    move+=_coords[p];
                    pm->push_back(move); //Save the movement
                }
            }
            if(p.getType() == ROOK){ //Gen move for rook
                vector<int> pos;
                pos_rook(i, color,&pos);
                for(int p : pos){
                    string move = _coords[i];
                    move+=_coords[p];
                    pm->push_back(move); //Save the movement
                }
            }
            if(p.getType() == QUEEN || p.getType() == KING){ //Gen move for queen and king (bishop+rook)
                vector<int> pos;
                pos_bishop(i, color,&pos);
                pos_rook(i, color,&pos);
                pos_roque(i, color, &pos); //for king
                for(int p : pos){
                    string move = _coords[i];
                    move+=_coords[p];
                    pm->push_back(move); //Save the movement
                }
            }
        }
    }
}

/** \brief Know if a piece can be attacked by a piece of the opponent side
 *
 * \param[in] pos The position of the piece we to know if is attacked
 *
 * \param[in] color The color of the opponent player
 *
 * \return **[bool]** return **true** if the piece is attacked
 */
bool Chess::isAttacked(int pos, bool color) {
    vector<string> moves;
    string pos2="";
    genMovesList(color, &moves);

    for(string m : moves){ //for each moves of the opponent
        pos2=m.substr(2,4);
        if(pos2 == _coords[pos]) //if the position of the piece is reachable by the opponent
            return true;
    }
    return false;
}

/** \brief Know if the king is in check use the \ref isAttacked method
 *
 * \param[in] color The color of the king we need to know if is in check
 *
 * \return **[bool]** return **true** if the king is in check
 */
bool Chess::inCheck(bool color) {
    int pos = 0;
    for (int i = 0; i < 64; ++i) {
        if(_cells[i].getType()==KING && _cells[i].getColor() == color){  //Find the king position
            pos=i;
            break;
        }
    }
    return isAttacked(pos, opponentColor(color)); //if is attacked the king is in check
}

/**
* \brief Undo the last move
*/
void Chess::undo() {
    setBoard(_history[_history.size()-1]); //Load the last movement
    _history.pop_back(); //remove from historic
}

/** \brief Make a move
 *
 * \param[in] move **e.g:*** "e2e4" the piece is moved from e2 to e4
 *
 * \return **[char]** return **1**  if the move is make without error\n
 *                    return **-1** if the selected piece is empty or is an opponent piece\n
 *                    return **-2** if the end position is not empty, depend on the rule\n
 *                    return **-3** if the king of side to move is in check
 */
char Chess::move(string move){
    string actualFEN = getBoard();
    int start = getIndex(move.substr(0, 2));
    int end = getIndex(move.substr(2, 2));
    const char promote = move.substr(4,1)[0]; //can be promote for example "e7e8Q" the pawn is moved from e7 to e8 the last cell and promote by a QUEEN

    Piece movedPiece = _cells[start]; //moved piece
    Piece takenPiece = _cells[end]; //taken piece, can be null : Piece()

    _moved = false;
    if(movedPiece.getColor() == _sideToMove && movedPiece.getType() != EMPTY){

        if(movedPiece.getType() == PAWN){

            vector<int> moves_index;
            pos_pawn(start, movedPiece.getColor(), &moves_index);

            if(movedPiece.getColor() == WHITE){
                for (auto mi:moves_index) {
                    if(mi == end){

                        //Promotion
                        if(end >= 0 && end <= 7){
                            if(symbolToType(promote) == ROOK)
                                _cells[end] = Piece(ROOK, _sideToMove);
                            else if(symbolToType(promote) == BISHOP)
                                _cells[end] = Piece(BISHOP, _sideToMove);
                            else if(symbolToType(promote) == KNIGHT)
                                _cells[end] = Piece(KNIGHT, _sideToMove);
                            else
                                _cells[end] = Piece(QUEEN, _sideToMove);

                        } else
                            _cells[end] = movedPiece;

                        _cells[start] = Piece();

                        //The movement is en passant
                        if(end == _en_passant){
                            _cells[end+8] = Piece();
                            _en_passant = -1;
                        }
                        else
                            _en_passant = -1;

                        //Gen en passant
                        if((start-16) == end){
                            if(_tab120[_tab64[end-1]] != -1 && _tab120[_tab64[end+1]] != -1){
                                Piece p1 = _cells[_tab120[_tab64[end-1]]];
                                Piece p2 = _cells[_tab120[_tab64[end+1]]];
                                if((p1.getType() == PAWN && p1.getColor() == BLACK) || (p2.getType() == PAWN && p2.getColor() == BLACK)){
                                    _en_passant = end+8;
                                }
                            }
                        }
                        _moved = true;
                        break;
                    }

                }

            }
            else{
                for (auto mi:moves_index) {
                    if(mi == end){

                        //Promotion
                        if(end >= 56 && end <= 63){
                            if(symbolToType(promote) == ROOK)
                                _cells[end] = Piece(ROOK, _sideToMove);
                            else if(symbolToType(promote) == BISHOP)
                                _cells[end] = Piece(BISHOP, _sideToMove);
                            else if(symbolToType(promote) == KNIGHT)
                                _cells[end] = Piece(KNIGHT, _sideToMove);
                            else
                                _cells[end] = Piece(QUEEN, _sideToMove);

                        } else
                            _cells[end] = movedPiece;

                        _cells[start] = Piece();

                        //The movement is en passant
                        if(end == _en_passant){
                            _cells[end-8] = Piece();
                            _en_passant = -1;
                        }
                        else
                            _en_passant = -1;

                        //Gen en passant
                        if((start+16) == end){
                            if(_tab120[_tab64[end-1]] != -1 && _tab120[_tab64[end+1]] != -1){
                                Piece p1 = _cells[_tab120[_tab64[end-1]]];
                                Piece p2 = _cells[_tab120[_tab64[end+1]]];
                                if((p1.getType() == PAWN && p1.getColor() == WHITE) || (p2.getType() == PAWN && p2.getColor() == WHITE)){
                                    _en_passant = end-8;
                                }
                            }
                        }
                        _moved = true;
                        break;
                    }
                }
            }

            if(!_moved)
                return -2;

            if(inCheck(movedPiece.getColor()))
                return -3;
        }
        else if(movedPiece.getType() == KNIGHT){
            vector<int> moves_index; //Possible moves
            pos_knight(start,movedPiece.getColor(), &moves_index);
            for (int mi : moves_index) {
                if(mi == end){
                    _cells[end] = movedPiece;
                    _cells[start] = Piece();
                    _moved = true;
                    break;
                }
            }
            if(!_moved)
                return -2;
            if(inCheck(movedPiece.getColor()))
                return -3;
        }
        else if(movedPiece.getType() == BISHOP){
            vector<int> pos_moves; //Possible moves
            pos_bishop(start,movedPiece.getColor(), &pos_moves);
            for (int move : pos_moves) {
                if(move == end){

                    _cells[end] = movedPiece;
                    _cells[start] = Piece();
                    _moved = true;
                    break;
                }
            }
            if(!_moved)
                return -2;
            if(inCheck(movedPiece.getColor()))
                return -3;
        }
        else if(movedPiece.getType() == ROOK){
            vector<int> pos_moves; //Possible moves
            pos_rook(start,movedPiece.getColor(), &pos_moves);
            for (int move : pos_moves) {
                if(move == end){

                    _cells[end] = movedPiece;
                    _cells[start] = Piece();

                    if(_sideToMove == WHITE){
                        if(start == 63)
                            _white_can_castle_63 = false;
                        if(start == 56)
                            _white_can_castle_56 = false;
                    }
                    else{
                        if(start == 7)
                            _black_can_castle_7 = false;
                        if(start == 0)
                            _black_can_castle_0 = false;
                    }

                    _moved = true;

                }
            }
            if(!_moved)
                return -2;
            if(inCheck(movedPiece.getColor()))
                return -3;


        }
        else if(movedPiece.getType() == QUEEN){
            vector<int> moves; //Possible moves
            pos_bishop(start,movedPiece.getColor(), &moves);
            pos_rook(start,movedPiece.getColor(), &moves);

            for (int mi : moves) {
                if(mi == end){
                    _cells[end] = movedPiece;
                    _cells[start] = Piece();
                    _moved = true;
                }
            }
            if(!_moved)
                return -2;
            if(inCheck(movedPiece.getColor()))
                return -3;
        }
        else if(movedPiece.getType() == KING){
            vector<int> moves; //Possible moves
            vector<int> castling_moves; //Possible moves
            pos_bishop(start,movedPiece.getColor(), &moves);
            pos_rook(start,movedPiece.getColor(), &moves);
            pos_roque(start, movedPiece.getColor(), &castling_moves);

            //Roque
            if(movedPiece.getColor() == WHITE){
                for (int mi : castling_moves) {
                    if(end == mi){
                        if(end == 62){ //Petit roque
                            _cells[end] = movedPiece;
                            _cells[start] = Piece();
                            _cells[61] = _cells[63];
                            _cells[63] = Piece();
                        }
                        if(end == 58){ //Grand Roque
                            _cells[end] = movedPiece;
                            _cells[start] = Piece();
                            _cells[59] = _cells[56];
                            _cells[56] = Piece();
                        }
                        _white_can_castle_63 = false;
                        _white_can_castle_56 = false;
                        _moved = true;
                        break;
                    }
                }
            }
            else if(movedPiece.getColor() == BLACK){
                for (int mi : castling_moves) {
                    if(end == mi){
                        if(end == 6){ //Petit roque
                            _cells[end] = movedPiece;
                            _cells[start] = Piece();
                            _cells[5] = _cells[7];
                            _cells[7] = Piece();
                        }
                        if(end == 2){ //Grand Roque
                            _cells[end] = movedPiece;
                            _cells[start] = Piece();
                            _cells[3] = _cells[0];
                            _cells[0] = Piece();
                        }

                        _black_can_castle_7 = false;
                        _black_can_castle_0 = false;
                        _moved = true;
                        break;
                    }
                }
            }

            if(!_moved){
                for (int mi : moves) {
                    if(mi == end){
                        _cells[end] = movedPiece;
                        _cells[start] = Piece();

                        if(movedPiece.getColor() == WHITE){
                            _white_can_castle_63 = false;
                            _white_can_castle_56 = false;
                        }
                        else if(movedPiece.getColor() == BLACK){
                            _black_can_castle_7 = false;
                            _black_can_castle_0 = false;
                        }
                        _moved = true;
                    }
                }
            }

            if(!_moved)
                return -2;
            if(inCheck(movedPiece.getColor()))
                return -3;
        }
    }
    else
        return -1;
    if(takenPiece.getType() != EMPTY)
        _ply = 0;
    return 1;
}


/** \brief Generate all legal move for a player/color
 *
 * \param[in] color The color of the player we want to generate all the possible move
 *
 * \param[out] pm The pointer of the vector/array to save the data
 *
 */
void Chess::genLegalMoves(bool color, vector<string> *pm){
    vector<string> moves;
    genMovesList(color, &moves);
    for (string m : moves) {

        save();
        int t = move(m);

        changeTrait();
        _move_number++;
        if(t == 1){
            pm->push_back(m);
            undo();
        }
        if(t != 1){
            undo();
        }


    }
}

/** \brief Know if the game is over
 *
 *\return **[bool]** return **true** if the game is over
 */
int Chess::gameover() {
    vector<string> moves;
    genLegalMoves(_sideToMove, &moves);

    if(moves.size() < 1){
        return true;
    }
    if(_ply >= 50) //100 half-moves so 50 hits
        return true;
    else if(_gameover)
        return true;
    else
        return false;

}

/** \brief Save the board with the fen description
 *
 */
void Chess::save() {
    _history.push_back(getBoard());
}

/** \brief Make a move
 *
 * \param[in] move **e.g:*** "e2e4" the piece is moved from e2 to e4
 *
 * \return **[char]** return **1**  if the move is make without error\n
 *                    return **-1** if the selected piece is empty or is an opponent piece\n
 *                    return **-2** if the end position is not empty, depend on the rule\n
 *                    return **-3** if the king of side to move is in check
 */
char Chess::makeMove(string m) {
    save();
    int t = move(m);

    changeTrait();
    _move_number++;
    _ply++;
    if(t != 1)
        undo();

    return t;
}

/** \brief Get the amount of legal for a side
 *
 *\return **[int]** return the amount of legal move available
 */
int Chess::getAmountOfLegalMove(bool color){
    vector<string> moves;
    genLegalMoves(color, &moves);
    return moves.size();
}

