const player = {
    socketId: "",
    fen: ""
}

const MAINMENU = 0;
const WAITING = 1
const BOARD = 2;
const DRAW = 3;
const PAT = 4;
const BW = 5;
const WW = 6;

var socket = io();
var selection = false;
var board = new Chess();

//When the window is loaded
window.onload = function() {

    menu2display(MAINMENU);                                 //Display the main menu

    var divs = document.querySelectorAll("div.cell");       //Select all the cells to add the event listener on click 
    for (i = 0; i < divs.length; ++i) {
        divs[i].id = i;
        divs[i].addEventListener('click', function(e) {
            clickCellEvent(e);                              //Call the function clickCellEvent to make the move
        });
    };
    drawBoard(board.getBoard());                            //Draw the board
}
/** \brief Display the board when the button play with A.I is clicked
* \And change the title of the page to player name vs AI
*/
function play(){
    menu2display(BOARD);
    document.title = document.getElementById("name").value + " vs AI";
}

/** \brief drawn the board with fen description
* \param[in] fen The board to set in fen format
*/
function drawBoard(fen){
    var cells = document.querySelectorAll("div.cell");
    let fen_parts = fen.split(" ");
    var rows = fen_parts[0].split("/");
    var i=0;
    var j=0;
    rows.forEach( row => {
        j=0;
        row.split("").forEach( k => {
            if(isNaN(k)){
                cells[i*8+j].innerHTML = k;
                j++;
            }
            else{
                for(var l=0; l<k; l++){
                    cells[i*8+j].innerHTML = "";
                    j++;
                }
            }
        })
        i++;
    });
}


/*ClickCellEvent */
var clicked = -1;
var pos;
var pawnToPromoteID=-1;

/** \brief Manage the click on the cells and control the move
* \param[in] e the event of click
*/
function clickCellEvent(e){
    //Remove for all cells the class "selected"
    var cells = document.querySelectorAll("div.cell");
    cells.forEach(cell => {
        cell.classList.remove("selected");
        cell.classList.remove("selected_enemy");
        cell.classList.remove("selected_empty");
    });
    
    //If a cell is selected
    if(clicked != -1){

        if(pos.includes(parseInt(e.target.id))){           //If the second click is an available move

            board.makeMove(clicked,parseInt(e.target.id)) //Make the move

            //If pawn go to the end of the board and can be promoted display the menu to choose the promotion
            if(board.get_piece(parseInt(e.target.id)).getType() == PAWN && parseInt(e.target.id) <= 7 && parseInt(e.target.id) >= 0){
                document.getElementById("promotepawn").style.display = "flex"; //none/flex
                document.getElementById("promotepawn").style.opacity = 1;
                document.getElementById("promotepawn").classList.remove("promoteinvisible");
                document.getElementById("promotepawn").classList.add("promotevisible");
                pawnToPromoteID = e.target.id;
                
            }
            
            //Remove the class "selected", "selected_enemy", "selected_empty" from all cells 
            cells.forEach(cell => {
                cell.classList.remove("selected");
                cell.classList.remove("selected_enemy");
                cell.classList.remove("selected_empty");
            });

            drawBoard(board.getBoard());        //Draw the board
            board.saveFens(board.getBoard());    //Save the move in fen description
            
            if(board.checkRepeat())             //Check if the move is a repeated
                menu2display(DRAW);
            

            if(pawnToPromoteID == -1)           //If  the pawn is  promoted or it doesn't need to be promoted
                sendData();
            
            clicked=-1;                         //Reset the clicked cell
            
        }
        else
            clicked=-1;                         //Reset the clicked cell
        
    }

    //If no cell is selected
    if(clicked == -1){
        //If the piece clicked is a piece of the player and it's his turn to play
        if(board.get_piece(e.target.id).getColor() != BLACK && board.get_side2move() == WHITE){
            pos = board.gen_legal_moves(parseInt(e.target.id))                                  //Generate the available moves
            clicked = parseInt(e.target.id)                                                     //Set the clicked cell

            //If the move is available add the class "selected" to the cell to highlight it
            pos.forEach(p => {
                //Hightlight the clicked cell
                document.getElementById(e.target.id).classList.add("selected");         

                //If the cells is an enemy cell add the class "selected_enemy" to highlight it in different color
                if(document.getElementById(p).innerHTML != ""){
                    document.getElementById(p).classList.add("selected_enemy");
                }
                else{
                    document.getElementById(p).classList.add("selected_empty");
                } 
            });
            
        }
    }
    
  
}


/** \brief Promote a pawn to a Knight
*/
function promoteToKnight(){
    board.get_piece(pawnToPromoteID).setType(KNIGHT);
    drawBoard(board.getBoard());
    document.getElementById("promotepawn").classList.add("promoteinvisible");
    document.getElementById("promotepawn").classList.remove("promotevisible");
    document.getElementById("promotepawn").style.opacity = 0; //none/flex
    pawnToPromoteID = -1;
    clicked=-1;
    sendData();
}

/** \brief Promote a pawn to a Bishop
*/
function promoteToBishop(){
    board.get_piece(pawnToPromoteID).setType(BISHOP);
    drawBoard(board.getBoard());
    document.getElementById("promotepawn").classList.add("promoteinvisible");
    document.getElementById("promotepawn").classList.remove("promotevisible");
    document.getElementById("promotepawn").style.opacity = 0; //none/flex
    pawnToPromoteID = -1;
    clicked=-1;
    sendData();

}

/** \brief Promote a pawn to a Rook
*/
function promoteToRook(){
    board.get_piece(pawnToPromoteID).setType(ROOK);
    drawBoard(board.getBoard());
    document.getElementById("promotepawn").classList.add("promoteinvisible");
    document.getElementById("promotepawn").classList.remove("promotevisible");
    document.getElementById("promotepawn").style.opacity = 0; //none/flex
    pawnToPromoteID = -1;
    clicked=-1;
    sendData();

}

/** \brief Promote a pawn to a Queen
*/
function promoteToQueen(){
    board.get_piece(pawnToPromoteID).setType(QUEEN);
    drawBoard(board.getBoard());
    document.getElementById("promotepawn").classList.add("promoteinvisible");
    document.getElementById("promotepawn").classList.remove("promotevisible");
    document.getElementById("promotepawn").style.opacity = 0; //none/flex
    pawnToPromoteID = -1;
    clicked=-1;
    sendData();
}

/** \brief forfait the game
*/
function forfeit(){
    menu2display(BW);
}

/** \brief Call the AI to play
*/
function sendData(){
    player.socketId = socket.id;
    player.fen = board.getBoard();

    socket.emit('play',player);
}

/** \brief Display the menu 
* \param[in] menu the menu to display
* MAINMENU, BOARD, WAITING, BW, WW, DRAW, PAT
*/
function menu2display(menu){
    switch(menu){
        case MAINMENU:
            document.getElementById("container-board").style.display = "none"; //none/flex
            document.getElementById("firstpage").style.display = "block"; //block
            document.getElementById("menu").style.display = "block"; //block
            document.getElementById("waiting").style.display = "none";  //none 
            document.getElementById("promotepawn").style.display = "none"; //none/block
            document.getElementById("forfight").style.display = "none"; //none/block
            document.getElementById("overlay1").style.display = "none"; 
            document.getElementById("overlay2").style.display = "none";
            break;
        case WAITING:
            document.getElementById("container-board").style.display = "none"; //none/flex
            document.getElementById("firstpage").style.display = "block"; //block
            document.getElementById("menu").style.display = "none"; //block
            document.getElementById("waiting").style.display = "block";  //none 
            document.getElementById("promotepawn").style.display = "none"; //none/block
            document.getElementById("forfight").style.display = "none"; //none/block
            document.getElementById("overlay1").style.display = "none"; 
            document.getElementById("overlay2").style.display = "none";

            break;
        case BOARD:
            document.getElementById("container-board").style.display = "flex"; //none/flex
            document.getElementById("firstpage").style.display = "none"; //block
            document.getElementById("menu").style.display = "none"; //block
            document.getElementById("waiting").style.display = "none";  //none 
            document.getElementById("promotepawn").style.display = "none"; //none/block
            document.getElementById("forfight").style.display = "block"; //none/block
            document.getElementById("overlay1").style.display = "none"; 
            document.getElementById("overlay2").style.display = "none";
            break;
        
        case DRAW:
            document.getElementById("overlay1").style.display = "none"; 
            document.getElementById("overlay2").style.display = "block";
            document.getElementById("text").innerHTML = "DRAW!";
            document.getElementById("forfight").style.display = "none"; //none/block
            break; 

        case PAT:
            document.getElementById("overlay1").style.display = "none"; 
            document.getElementById("overlay2").style.display = "block";
            document.getElementById("text").innerHTML = "PAT!";
            document.getElementById("forfight").style.display = "none"; //none/block

            break;
        
        case BW:
            document.getElementById("overlay1").style.display = "none";   //Ne pas jouer l'animation de victoire
            document.getElementById("overlay2").style.display = "block"; //Afficher l'overlay de fin de partie
            document.getElementById("text").innerHTML = "Black won!"; //Afficher le message de défaite
            document.getElementById("forfight").style.display = "none"; //none/block

            break;
        
        case WW:
            document.getElementById("overlay1").style.display = "block"; //Jouer l'annimation   
            document.getElementById("overlay2").style.display = "block";//Afficher l'overlay de fin de partie
            document.getElementById("text").innerHTML = "White won!"; //Afficher le message de victoire
            document.getElementById("forfight").style.display = "none"; //none/block
            break;


    };
}

/** \brief Display the main menu 
*\ reset the game
*/
function mainMenu(){
    menu2display(MAINMENU);
    board.setBoard("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
    drawBoard(board.getBoard());

}

/** \When the sever send the board, with ai move, update the board
*\ reset the game
*/
socket.on('play', function(fen){
    board.setBoard(fen);
    drawBoard(fen);
    if(parseInt(fen.split(" ")[4])>=50){
        menu2display(DRAW);
    }
});

/** \When the sever send the status of the game because the game is over
*\  Display the correct menu depending on the status
*/
socket.on('end', function(status) {
    if(status == -1)
        menu2display(BW);
    
    else if(status == -2)
        menu2display(WW);
    
    else if(status == 0)
        menu2display(PAT);
    
});