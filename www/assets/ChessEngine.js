
const NONE = -1;
const WHITE = 0; 
const BLACK = 1; 

const KING = "K";
const QUEEN = "Q";
const ROOK = "R";
const BISHOP = "B";
const KNIGHT = "N";
const PAWN = "P";
const EMPTY = "";

class Piece {

    #color = NONE;
    #type = EMPTY;

    /**
    *\brief      Constructor of the class Piece. Create a piece with a type and a color
    */
    constructor(type, color) {
        this.#color = color;
        this.#type = type;    
    }

    /** \brief Set piece type
    */
    setType(type){ this.#type = type; }

    /** \brief Get piece type
     * \return the type of the piece
    */
    getType(){ return this.#type; }

    /** \brief Set piece color
    */
    setColor(color){ this.#color = color; }

    /** \brief Get piece color
     * \return the color of the piece
    */
    getColor(){ return this.#color; }

    /** \brief Get piece symbol
     * \return the symbol of the piece
    */
    getSymbol(){
        if(this.#color == WHITE || this.#color == NONE)
            return this.#type;
        else
            return this.#type.toLocaleLowerCase();     
    }

}
class Chess {
    #history = [];
    #fens= [];
    #tab120 = [
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1,  0,  1,  2,  3,  4,  5,  6,  7, -1,
        -1,  8,  9, 10, 11, 12, 13, 14, 15, -1,
        -1, 16, 17, 18, 19, 20, 21, 22, 23, -1,
        -1, 24, 25, 26, 27, 28, 29, 30, 31, -1,
        -1, 32, 33, 34, 35, 36, 37, 38, 39, -1,
        -1, 40, 41, 42, 43, 44, 45, 46, 47, -1,
        -1, 48, 49, 50, 51, 52, 53, 54, 55, -1,
        -1, 56, 57, 58, 59, 60, 61, 62, 63, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
    ];

    #tab64 = [
        21, 22, 23, 24, 25, 26, 27, 28,
        31, 32, 33, 34, 35, 36, 37, 38,
        41, 42, 43, 44, 45, 46, 47, 48,
        51, 52, 53, 54, 55, 56, 57, 58,
        61, 62, 63, 64, 65, 66, 67, 68,
        71, 72, 73, 74, 75, 76, 77, 78,
        81, 82, 83, 84, 85, 86, 87, 88,
        91, 92, 93, 94, 95, 96, 97, 98
    ];

    #coords = 
    [
        "a8","b8","c8","d8","e8","f8","g8","h8",
        "a7","b7","c7","d7","e7","f7","g7","h7",
        "a6","b6","c6","d6","e6","f6","g6","h6",
        "a5","b5","c5","d5","e5","f5","g5","h5",
        "a4","b4","c4","d4","e4","f4","g4","h4",
        "a3","b3","c3","d3","e3","f3","g3","h3",
        "a2","b2","c2","d2","e2","f2","g2","h2",
        "a1","b1","c1","d1","e1","f1","g1","h1"
    ];
    
    #cells = 
    [
        new Piece(ROOK,BLACK),new Piece(KNIGHT,BLACK),new Piece(BISHOP,BLACK),new Piece(QUEEN,BLACK),new Piece(KING,BLACK),new Piece(BISHOP,BLACK),new Piece(KNIGHT,BLACK),new Piece(ROOK,BLACK),
        new Piece(PAWN,BLACK),new Piece(PAWN,BLACK),new Piece(PAWN,BLACK),new Piece(PAWN,BLACK),new Piece(PAWN,BLACK),new Piece(PAWN,BLACK),new Piece(PAWN,BLACK),new Piece(PAWN,BLACK),

        new Piece(EMPTY,NONE),new Piece(EMPTY,NONE),new Piece(EMPTY,NONE),new Piece(EMPTY,NONE),new Piece(EMPTY,NONE),new Piece(EMPTY,NONE),new Piece(EMPTY,NONE),new Piece(EMPTY,NONE),
        new Piece(EMPTY,NONE),new Piece(EMPTY,NONE),new Piece(EMPTY,NONE),new Piece(EMPTY,NONE),new Piece(EMPTY,NONE),new Piece(EMPTY,NONE),new Piece(EMPTY,NONE),new Piece(EMPTY,NONE),
        new Piece(EMPTY,NONE),new Piece(EMPTY,NONE),new Piece(EMPTY,NONE),new Piece(EMPTY,NONE),new Piece(EMPTY,NONE),new Piece(EMPTY,NONE),new Piece(EMPTY,NONE),new Piece(EMPTY,NONE),
        new Piece(EMPTY,NONE),new Piece(EMPTY,NONE),new Piece(EMPTY,NONE),new Piece(EMPTY,NONE),new Piece(EMPTY,NONE),new Piece(EMPTY,NONE),new Piece(EMPTY,NONE),new Piece(EMPTY,NONE),

        new Piece(PAWN,WHITE),new Piece(PAWN,WHITE),new Piece(PAWN,WHITE),new Piece(PAWN,WHITE),new Piece(PAWN,WHITE),new Piece(PAWN,WHITE),new Piece(PAWN,WHITE),new Piece(PAWN,WHITE),
        new Piece(ROOK,WHITE),new Piece(KNIGHT,WHITE),new Piece(BISHOP,WHITE),new Piece(QUEEN,WHITE),new Piece(KING,WHITE),new Piece(BISHOP,WHITE),new Piece(KNIGHT,WHITE),new Piece(ROOK,WHITE)
    ];
    
    #move_vectors_bishop = [-11,-9,9,11];
    #move_vectors_knight = [-21,-19,-12,-8,8,12,19,21];
    #move_vectors_rook   = [-10,-1,1,10];

    #side2move = WHITE;
    #enpassant = -1;
    #ply=0;
    #move_number=1;
    
    #whiteCanCastle56 = true;
    #whiteCanCastle63 = true;
    #blackCanCastle0 = true;
    #blackCanCastle7 = true;

    constructor(){
    }

    /** \brief Set the chessboard with a fen
    *
    * \param[in] fen The board to set in fen format
    *
    */
    setBoard(fen){
        let fen_parts = fen.split(" ");
        var rows = fen_parts[0].split("/");
        var i=0;
        var j=0;
        rows.forEach( row => {
            j=0;
            row.split("").forEach( piece => {
                if(isNaN(piece)){
                    this.#cells[i*8+j].setType(piece.toUpperCase());
                    if(piece.toUpperCase()==piece)
                        this.#cells[i*8+j].setColor(WHITE);
                    else
                        this.#cells[i*8+j].setColor(BLACK);
                    j++;
                }
                else{
                    for(var l=0; l<parseInt(piece); l++){
                        this.#cells[i*8+j].setType(EMPTY);
                        this.#cells[i*8+j].setColor(NONE);
                        j++;
                    }
                }
            })
            i++;
        });

        this.#side2move = fen_parts[1]=="w"?WHITE:BLACK;

        /*Castling right*/
        let castling = fen_parts[2];
        if(castling != "-"){
            if(castling.indexOf("K")>-1)
                this.#whiteCanCastle63 = true;
            else
                this.#whiteCanCastle63 = false;

            if(castling.indexOf("Q")>-1)
                this.#whiteCanCastle56 = true;
            else
                this.#whiteCanCastle56 = false;

            if(castling.indexOf("k")>-1)
                this.#blackCanCastle7 = true;
            else
                this.#blackCanCastle7 = false;

            if(castling.indexOf("q")>-1)
                this.#blackCanCastle0 = true;
            else
                this.#blackCanCastle0 = false;
        }
        else{
            this.#whiteCanCastle63 = false;
            this.#whiteCanCastle56 = false;
            this.#blackCanCastle7 = false;
            this.#blackCanCastle0 = false;
        }

        /*Enpassant*/
        let enpassant = fen_parts[3];
        if(this.#coords.indexOf(enpassant)>-1)
            this.#enpassant = this.#coords.indexOf(enpassant);
        
        else
            this.#enpassant = -1;

        /*Move number*/
        this.#ply = parseInt(fen_parts[4]);

        /*Half move clock*/
        this.#move_number = parseInt(fen_parts[5]);

    }

    /** \brief Get the board
     * \return the board in fen format
    */
    getBoard(){
        var fen_board = "";
        var emptySq = 0;
        this.#cells.forEach(function(cell, i){
            if(emptySq==8){
                fen_board+="8";
                emptySq=0;
            }
            if(i && (i%8==0)){
                if(emptySq>0){
                    fen_board+=emptySq;
                    emptySq=0;
                }
                fen_board+="/";
            }
            if(cell.getType()==EMPTY){
                emptySq++;
            }
            else{
                if(emptySq>0){
                    fen_board+=emptySq;
                    emptySq=0;
                }
                fen_board+=cell.getSymbol();
            }
        });
        if(emptySq>0)
            fen_board+=emptySq;

        fen_board+= " "
        if(this.#side2move == WHITE)
            fen_board+= "w";
        else
            fen_board+= "b";

        fen_board+=" ";
        if(this.#whiteCanCastle63)
            fen_board+="K";
        if(this.#whiteCanCastle56)
            fen_board+="Q";
        if(this.#blackCanCastle7)
            fen_board+="k";
        if(this.#blackCanCastle0)
            fen_board+="q";
        
        if(!this.#whiteCanCastle63 && !this.#whiteCanCastle56 && !this.#blackCanCastle7 && !this.#blackCanCastle0)
            fen_board+="-";


        fen_board+=" "
        if(this.#enpassant == -1)
            fen_board+= "-";
        else
            fen_board+= this.#coords[this.#enpassant];
        
        fen_board+=" "+this.#ply+" "+this.#move_number;

        

        return fen_board;
    }

    /** \brief Get the opponent color
     * \return the color of the opponent
    */
    oppColor(color){
        if(color==WHITE)
            return BLACK;
        else
            return WHITE;
    }

    /** \brief Generate the possible cell where the pawn can go
    *
    * \param[in] index The index/location of the pawn
    *
    */
    pos_pawn(index){
        var moves = [];
        var color = this.#cells[index].getColor();

        if(this.#cells[index].getType()==PAWN){
            if(color == WHITE){
                if(this.#cells[index-8].getType()==EMPTY){
                        moves.push(index-8);
                        if(index>=48 && index <= 55 && this.#cells[index-16].getType()==EMPTY)
                            moves.push(index-16);
                }

                if(this.#tab120[this.#tab64[index]-11]!=-1){
                    if(this.#cells[index-9].getColor()==this.oppColor(color) || (index-9)==this.#enpassant && this.#enpassant >= 16 && this.#enpassant <=23)
                        moves.push(index-9);
                }
                if(this.#tab120[this.#tab64[index]-9]!=-1){
                    if(this.#cells[index-7].getColor()==this.oppColor(color)|| (index-7)==this.#enpassant && this.#enpassant >= 16 && this.#enpassant <=23)
                        moves.push(index-7);
                }
                
            }
            else{
                
                if(this.#cells[index+8].getType()==EMPTY){
                        moves.push(index+8);
                        if(index>=8 && index <= 15 && this.#cells[index+16].getType()==EMPTY)
                            moves.push(index+16);
                }
                
                
                if(this.#tab120[this.#tab64[index]+11]!=-1){
                    if(this.#cells[index+9].getColor()==this.oppColor(color) || (index+9)==this.#enpassant && this.#enpassant >= 40 && this.#enpassant <=47)
                        moves.push(index+9);
                }
                if(this.#tab120[this.#tab64[index]+9]!=-1){
                    if(this.#cells[index+7].getColor()==this.oppColor(color)|| (index+7)==this.#enpassant && this.#enpassant >= 40 && this.#enpassant <=47)
                        moves.push(index+7);
                }
            }
            
        }
        


        return moves;
    }

    /** \brief Generate the possible cell where the knight can go
    *
    * \param[in] index The index/location of the knight
    *
    */
    pos_knight(index){
        var moves = [];
        var i = 0;
        this.#move_vectors_knight.forEach( move => {
            var c = this.#tab120[this.#tab64[index]-move];
            if(c != -1){
                if(this.#cells[c].getType()==EMPTY || this.#cells[c].getColor()!=this.#cells[index].getColor()){
                    moves.push(c);
                }
            }
        });
        return moves;
    }

    /** \brief Generate the possible cell where the bishop can go
    *
    * \param[in] index The index/location of the bishop
    *
    */
    pos_bishop(index){
        var moves = [];
        var i = 1;
        this.#move_vectors_bishop.forEach( move => {
            i=1;
            while(true){
                var n = this.#tab120[this.#tab64[index]+(move*i)];
                if(n != -1){
                    if(this.#cells[n].getType() === EMPTY || this.#cells[n].getColor()!=this.#cells[index].getColor())
                        moves.push(n);
                }
                else
                  break;
                    
                if(this.#cells[n].getType() != EMPTY || this.#cells[index].getType() == KING)
                    break;

                i++;
            }
        });
        return moves;
    }

    /** \brief Generate the possible cell where the rook can go
    *
    * \param[in] index The index/location of the rook
    *
    */
    pos_rook(index){
        var moves = [];
        var i = 1;
        this.#move_vectors_rook.forEach( move => {
            i=1;
            while(true){
                var n = this.#tab120[this.#tab64[index]+(move*i)];
                if(n != -1){
                    if(this.#cells[n].getType() === EMPTY || this.#cells[n].getColor()!=this.#cells[index].getColor())
                        moves.push(n);
                    
                }
                else
                  break;
                    
                if(this.#cells[n].getType() != EMPTY  || this.#cells[index].getType() == KING)
                    break;

                i++;
            }
        });
        
        return moves;
    }

    /** \brief Generate the move if castling is possible
    *
    * \param[in] index The index/location of the castling
    *
    */
    pos_roque(index){
        var moves = [];

        if(this.#cells[index].getType() == KING){
            if(this.#cells[index].getColor() == WHITE){
                if(this.#whiteCanCastle63 && this.#cells[60].getType() == KING && this.#cells[61].getType() == EMPTY  && this.#cells[62].getType() == EMPTY && this.#cells[63].getType() == ROOK){
                    moves.push(62);
                }
                if(this.#whiteCanCastle56 && this.#cells[60].getType() == KING && this.#cells[59].getType() == EMPTY  && this.#cells[58].getType() == EMPTY && this.#cells[57].getType() == EMPTY && this.#cells[56].getType() == ROOK){
                    moves.push(58);
                }
            }
            else{
                if(this.#blackCanCastle7 && this.#cells[4].getType() == KING && this.#cells[5].getType() == EMPTY  && this.#cells[6].getType() == EMPTY && this.#cells[7].getType() == ROOK){
                    moves.push(6);
                }
                if(this.#whiteCanCastle56 && this.#cells[4].getType() == KING && this.#cells[3].getType() == EMPTY  && this.#cells[2].getType() == EMPTY && this.#cells[1].getType() == EMPTY && this.#cells[0].getType() == ROOK){
                    moves.push(2);
                }
            }
        }
        return moves;
    }

    /** \brief Make a move
    *
    * \param[in] start The index/location of the piece
    * \param[in] end The index/location of the destination
    * 
    */
    makeMove(start, end){
        
        var movedPiece = this.#cells[start]

        
        
        if(movedPiece.getType() == PAWN){
            
            if(movedPiece.getColor() == WHITE){
                if(end == this.#enpassant){
                    this.#cells[end+8].setType(EMPTY);
                    this.#cells[end+8].setColor(NONE);
                    this.#enpassant = -1;
                }    

                //Gen en passant move
                if((start-16) == end){
                    if(this.#tab120[this.#tab64[end]-1] != -1 &&  this.#tab120[this.#tab64[end]+1] != -1){
                        this.#enpassant = end;
                        var p1 = this.#cells[this.#tab120[this.#tab64[end]-1]];
                        var p2 = this.#cells[this.#tab120[this.#tab64[end]+1]];
                        if((p1.getType() == PAWN && p1.getColor() == BLACK) || (p2.getType() == PAWN && p2.getColor() == BLACK)){
                            this.#enpassant = end+8;
                        }

                        
                    }
                }
            }
            else{
                if(end == this.#enpassant){
                    this.#cells[end-8].setType(EMPTY);
                    this.#cells[end-8].setColor(NONE);
                    this.#enpassant = -1;
                }  
                //Gen en passant move
                if((start+16) == end){
                    if(this.#tab120[this.#tab64[end]-1] != -1 &&  this.#tab120[this.#tab64[end]+1] != -1){
                        this.#enpassant = end;
                        var p1 = this.#cells[this.#tab120[this.#tab64[end]-1]];
                        var p2 = this.#cells[this.#tab120[this.#tab64[end]+1]];
                        if((p1.getType() == PAWN && p1.getColor() == BLACK) || (p2.getType() == PAWN && p2.getColor() == BLACK)){
                            this.#enpassant = end-8;
                        }

                        
                    }
                }  
            }

        }
        else if(movedPiece.getType() == ROOK ){
            if(movedPiece.getColor() == WHITE){
                if(start == 63){
                    this.#whiteCanCastle63 = false;
                }
                else if(start == 56){
                    this.#whiteCanCastle56 = false;
                }
            }
            else{
                if(start == 7){
                    this.#blackCanCastle7 = false;
                }
                else if(start == 0){
                    this.#blackCanCastle0 = false;
                }
            }
        }
        else if(movedPiece.getType() == KING){

            
            if(movedPiece.getColor() == WHITE){
                if(end == 62 && this.#whiteCanCastle63){
                    this.#cells[61].setType(ROOK);
                    this.#cells[61].setColor(WHITE);
                    this.#cells[63].setType(EMPTY);
                    this.#cells[63].setColor(NONE);
                }
                if(end == 58 && this.#whiteCanCastle56){
                    this.#cells[59].setType(ROOK);
                    this.#cells[59].setColor(WHITE);
                    this.#cells[56].setType(EMPTY);
                    this.#cells[56].setColor(NONE);
                }
                this.#whiteCanCastle63 = false;
                this.#whiteCanCastle56 = false;
            }
            else{
                if(end == 6 && this.#blackCanCastle7){
                    this.#cells[5].setType(ROOK);
                    this.#cells[5].setColor(BLACK);
                    this.#cells[7].setType(EMPTY);
                    this.#cells[7].setColor(NONE);
                }
                else if(end == 1 && this.#blackCanCastle0){
                    this.#cells[2].setType(ROOK);
                    this.#cells[2].setColor(BLACK);
                    this.#cells[0].setType(EMPTY);
                    this.#cells[0].setColor(NONE);
                }

                this.#blackCanCastle7 = false;
                this.#blackCanCastle0 = false;
            }

        }
        if(this.#cells[end].getType() != EMPTY){
            
        }
        this.#cells[end].setType(this.#cells[start].getType());
        this.#cells[end].setColor(this.#cells[start].getColor());
        this.#cells[start].setType(EMPTY);
        this.#cells[start].setColor(NONE);

        if(this.#side2move == BLACK)
            this.#side2move = WHITE;
        else
            this.#side2move = BLACK; //TODO: change this to black
        
        

    }

    /** \brief Save the board with the fen description
    *
    */
    saveBoard(){
        this.#history.push(this.getBoard());
    }

    /**
    * \brief Undo the last move
    */
    undo(){
        if(this.#history.length>0)
            this.setBoard(this.#history.pop());
        
    }

    /** \brief Generate all possible move for a  piece
     * 
     *
     * \param[in] int The index of the piece we want to generate all the possible move
     *
     * return A list of all the possible move for the piece
     *
     */
    gen_moves(index){
        var moves = [];
        switch(this.#cells[index].getType()){
            case PAWN:
                moves = this.pos_pawn(index);
                break;
            case KNIGHT:
                moves = this.pos_knight(index);
                break;
            case BISHOP:
                moves = this.pos_bishop(index);
                break;
            case ROOK:
                moves = this.pos_rook(index);
                break;
            case QUEEN:
                moves = this.pos_bishop(index).concat(this.pos_rook(index));
                break;
            case KING:
                moves = this.pos_bishop(index).concat(this.pos_rook(index)).concat(this.pos_roque(index));
        }
        return moves;
    }

    /** \brief Generate all possible move for a player/color
     *
     * \param[in] color The color of the player we want to generate all the possible move
     *
     * return A list of all the possible move for the player
     *
     */
    gen_all_moves(color){
        var moves = [];
        for(var i=0; i<64; i++){
            if(this.#cells[i].getColor()==color){
                moves = moves.concat(this.gen_moves(i));
            }
        }
        return moves;
    }


    /** \brief Know if a piece can be attacked by a piece of the opponent side
    *
    * \param[in] pos The position of the piece we to know if is attacked
    *
    * \param[in] color The color of the opponent player
    *
    * \return **[bool]** return **true** if the piece is attacked
    */
    isAttacked(pos, oppColor){
        var moves = this.gen_all_moves(oppColor);
        var isAttacked = false;
        moves.forEach( move => {
            if(move == pos){
                isAttacked = true;
            }
            
        });
        return isAttacked;
        
    }

    /** \brief Know if the king is in check use the \ref isAttacked method
    *
    * \param[in] color The color of the king we need to know if is in check
    *
    * \return **[bool]** return **true** if the king is in check
    */
    isInCheck(color){
        var king = -1;
        for(var i=0; i<64; i++){
            if(this.#cells[i].getType()==KING && this.#cells[i].getColor()==color){
                king = i;
                break;
            }
        }
        return this.isAttacked(king, this.oppColor(color));

    }

    /** \brief Generate all legal move for a player/color
    *
    * \param[in] color The color of the player we want to generate all the possible move
    *
    * \param[out] pm The pointer of the vector/array to save the data
    *
    */
    gen_legal_moves(index){
        var c = this.#cells[index].getColor();
        var moves = this.gen_moves(index);
        var legal_moves = [];

        moves.forEach( move => {
            this.saveBoard();
            this.makeMove(index, move);
            
            if(!this.isInCheck(c))
                legal_moves.push(move);
            this.undo();
        });
        return legal_moves;
    }

    /** \brief Get piece type
    * 
    * \param[in] index The index of the piece
    * \return the piece
    * 
    */
    get_piece(index){
        return this.#cells[index];
    }

    /** \brief Get the side to move
    * 
    * \return the color of the player who is playing
    * 
    */
    get_side2move(){
        return this.#side2move;
    }

    /** \brief Get the coords of an index
    * 
    * \return the coords of the index e.g: 0 -> a8
    * 
    */
    getCoords(c){
        return this.#coords[c];
    }

    /** \brief Save the played move in fen description
    */
    saveFens(fen){
        this.#fens.push(fen);
    }

    /** \brief Know if repetition is detected
    *
    * \return **[bool]** return **true** if repetition is detected
    */
    checkRepeat(){
        if(this.#fens.length > 3 && this.#fens[this.#fens.length-1].split(" ")[0] == this.#fens[this.#fens.length-3].split(" ")[0])
            return true;
        
        else
            return false
    }

}