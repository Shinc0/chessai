
const { Socket } = require('engine.io');
const { exec } = require("child_process");

var express = require('express')
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
const path = require('path');

//Start the server on port 3000
http.listen(3000, () => {
    console.log('Server listening at port %d', 3000);
});

//Set the static files location
app.use(express.static(path.join(__dirname, 'www')));


io.on('connection', function(socket){
      
    console.log("\x1b[32m[Connection]\x1b[0m " + socket.id);
    

    //On play
    socket.on('play', function(player){
        console.log("\x1b[32m[Play]\x1b[0m "+ player.socketId);

        //Call the AI with a shell command
        exec("./Chess \"" + player.fen +"\"",(error, stdout, stderr) => {
            //If problem with the command print the error and exit
            if (error) { 
                console.log(`error: ${error.message}`);
                return;
            }
            //If the script return a error print the error and exit
            if (stderr) {
                console.log(`stderr: ${stderr}`);
                return;
            }
            //Else send the fen to the client
            if(stdout != "-1" )
                io.to(player.socketId).emit('whiteIsCheck', stdout);

            
            if(stdout != -1 && stdout != -2 && stdout  != 0)
            
                io.to(player.socketId).emit('play', stdout);
            
            else
                io.to(player.socketId).emit('end', stdout);
            
            
                
        });
    });
});

